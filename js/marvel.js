
/********************************************************************************************************************
DECLARAÇÃO DE VARIÁVEIS 
*********************************************************************************************************************/

var d;
var dia;
var mes;;
var ano;
var data;


/********************************************************************************************************************
FUNÇÃO PARA RECUPERAR AS PERSONAGENS
*********************************************************************************************************************/

var listaCharacters = function(){
	var characterAPI = "http://gateway.marvel.com/v1/public/characters?ts=1&apikey=acc7affcc825626481537de1dc9c952f&hash=1ee201ab12b30ef66a687ff6371f37d4";
	

	$('#conteudo').empty();

	$.ajax({
			url: characterAPI,
			type: "GET",

			beforeSend: function(){
				$("#mensagem").text("Carregando");
				$("#espera").css("display", "block");
			},

			complete: function(response){
				$("#novo_item").css("display", "block");
				$("#espera").hide();
				$("#mensagem").text("Personagens");
				
				console.log(response.responseJSON.data.results);
				$.each(response.responseJSON.data.results, function(i, item){
					d = new Date(item.modified);
					dia = d.getDate();
					mes = d.getMonth();
					ano = d.getFullYear();

					data = ""+dia+"/"+(mes+1)+"/"+ano;
					
					$('#conteudo').append('<tr id="'+item.id+'"><td><b>Nome: </b>'+item.name+'<br><b>Última modificação: </b>'+data+'</td>'+
						'<td><button class="btn btn-danger" onclick="myfunction('+item.id+');">Remover</button></td></tr>');
				});
	
			},
			error: function(){
				$("#mensagem").text("Não foi possivel carregar informações sobre as personagens.");
			}
		});
	
	
}


/********************************************************************************************************************
FUNÇÃO PARA RECUPERAR OS AUTORES
*********************************************************************************************************************/

var listaAutores = function(){
	var autorsAPI = "http://gateway.marvel.com/v1/public/creators?ts=1&apikey=acc7affcc825626481537de1dc9c952f&hash=1ee201ab12b30ef66a687ff6371f37d4";
	

	$('#conteudo').empty();

	$.ajax({
			url: autorsAPI,
			type: "GET",

			beforeSend: function(){
				$("#mensagem").text("Carregando");
				$("#espera").css("display", "block");
			},

			complete: function(response){
				$("#espera").hide();
				$("#mensagem").text("Autores");
				$.each(response.responseJSON.data.results, function(i, item){
					d = new Date(item.modified);
					dia = d.getDate();
					mes = d.getMonth();
					ano = d.getFullYear();

					data = ""+dia+"/"+(mes+1)+"/"+ano;

					$('#conteudo').append('<tr id="'+item.id+'"><td><b>Nome: </b>'+item.firstName+'<br><b>Última modificação: </b>'+data+'</td>'+
						'<td><button class=" btn btn-danger" onclick="myfunction('+item.id+');">Remover</button></td></tr>');
					
				});
			},
			error: function(){
				$("#mensagem").text("Não foi possivel carregar informações sobre os autores.");
			}
		});

	
}

/********************************************************************************************************************
FUNÇÃO PARA RECUPERAR AS SERIES
*********************************************************************************************************************/

var listaSeries = function(){
	var seriesAPI = "http://gateway.marvel.com/v1/public/series?ts=1&apikey=acc7affcc825626481537de1dc9c952f&hash=1ee201ab12b30ef66a687ff6371f37d4";
	
	$('#conteudo').empty();
	
	$.ajax({
			url: seriesAPI,
			type: "GET",

			beforeSend: function(){
				$("#mensagem").text("Carregando");
				$("#espera").css("display", "block");
			},

			complete: function(response){
				$("#espera").hide();
				$("#mensagem").text("Series");
				console.log(response.responseJSON.data.results);

				$.each(response.responseJSON.data.results, function(i, item){
					d = new Date(item.modified);
					dia = d.getDate();
					mes = d.getMonth();
					ano = d.getFullYear();

					data = ""+dia+"/"+(mes+1)+"/"+ano;
					
					$('#conteudo').append('<tr id="'+item.id+'"><td><b>Título: </b>'+item.title+'<br><b>Última modificação: </b>'+data+'</td>'+
						'<td><button class=" btn btn-danger" onclick="myfunction('+item.id+');">Remover</button></td></tr>');
					
				});
				
			},
			error: function(){
				$("#mensagem").text("Não foi possivel carregar informações sobre as series.");
			}
	});




	
}


/********************************************************************************************************************
FUNÇÃO PARA REMOVER ITEM DA LISTA
*********************************************************************************************************************/

function myfunction(id_name){
	$("#"+id_name).remove();
}



/********************************************************************************************************************
CHAMADA DAS FUNÇÕES NO MENU
*********************************************************************************************************************/

$(document).ready(function(){
	
	$("#mensagem").text("Clique no menu acima e descubra informações sobre o universo Marvel!");

	$("#personagens").click(function(){
		listaCharacters();
	});

	$("#autores").click(function(){
		listaAutores();
	});

	$("#seriados").click(function(){
		listaSeries();
	});
});